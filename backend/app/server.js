//Dépendencies
const express = require('express');
const mongoose = require('mongoose');

//Local dependencies
const bodyParser = require ('body-parser');
const compression = require('compression');
const cors = require('cors');
const helmet = require('helmet');

//Core
const config = require('./config.js')
const routes = require('./controllers/routes.js')


/**
 * Server
 */
module.exports = class Server {

    constructor() {
        this.app = express();
        this.config = config[process.argv[2]] || config.development;
    }

    /**
     * connexion bdd
     * @return {Object} connect
     */
    dbconnect() {
        const host = this.config.mongodb;
        const connect = mongoose.createConnection(host);

    connect.on('error', (err) => {
        setTimeout(() => {
            console.log('[ERROR] users api dbconnect() -> mongodb error');
            this.connect = this.dbconnect(host);
        }, 3000)

        console.error(`[ERROR] api dbConnect() -> ${err}`)
    })

    connect.on('disconnected', (err) => {
        setTimeout(() => {
            console.log('[DISCONNECTED] users api dbconnect() -> mongodb disconnected');
            this.connect = this.dbconnect(host);
        }, 3000)
    })

    process.on('SIGINT', () => {
        connect.close(() => {
        console.log('[API END PROCESS] users api dbConnect() -> close mongodb connection')
        process.exit(0)
        })
    })

    return connect;

    }

    /**
     * Middleware
     */
    middleware() {
        this.app.use(compression())
        this.app.use(cors())
        this.app.use(bodyParser.urlencoded({'extended': true}))
        this.app.use(bodyParser.json())
    }

    /**
     * Routes
     */
    routes() {
        new routes.student.Show(this.app, this.connect, this.config)
        new routes.student.Post(this.app, this.connect, this.config)
        new routes.student.Put(this.app, this.connect, this.config)
        new routes.student.Delete(this.app, this.connect, this.config)
        new routes.students.ShowAll(this.app, this.connect, this.config)

    // If route not exist
    this.app.use((req,res) => {
        res.status(404).json({
            'code': 404,
            'message': 'Not Found'
        })
    })

    }

    /**
     * Security
     */
    security() {
        this.app.use(helmet());
        this.app.disable('x-powered-by');
    }

    /**
     * Run
     */
    run() {
        try {
        this.connect = this.dbconnect();
        this.security();
        this.middleware();
        this.routes();

        this.app.listen(3000);
        } catch (err) {
            console.error(`[ERROR] Server -> ${err}`)
        } 
    }
}