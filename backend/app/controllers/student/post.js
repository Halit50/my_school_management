const studentModel = require('../../models/student.js');
const bcrypt = require('bcrypt');
const Student = require('../../models/student.js');
// Package qui permet de générer un token 
const jwt = require('jsonwebtoken');

class Post {
    /**
     * @constructor
     * @param {Object} app
     * @param {Object} config
     */
    constructor (app, connect, config) {
        this.app = app;
        this.studentModel = connect.model('Student', studentModel);
        this.run()
    }

    /**
     * Middleware
     */
    middleware() {
        this.app.post('/student/', (req, res) => {
            try {
                const studentModel = new this.studentModel(req.body)

                studentModel.save().then(student => {
                    res.status(200).json(student || {})
                }).catch(() => {
                    res.status(200).json({})
                });

            } catch(err) {
                console.error(`[ERROR] student/post -> ${err}`)
                res.status(400).json({
                    code: 400,
                    message:'Bad request'
                })
            }
        })
    }

    /**
     * Run
     */
    run () {
        this.middleware()
    }
}

module.exports = Post;
