const studentModel = require('../../models/student.js');

class Delete {
    /**
     * @constructor
     * @param {Object} app
     * @param {Object} config
     */
    constructor (app, connect, config) {
        this.app = app;
        this.studentModel = connect.model('Student', studentModel);
        this.run()
    }

    /**
     * Middleware
     */
    middleware() {
        this.app.delete('/student/delete/:id', (req, res) => {
            try {
                this.studentModel.deleteOne({_if: req.params.id}).then((student) => {
                    res.status(200).json({student})
                }).catch(() => {
                    res.status(200).json({})
                });
            } catch(err) {
                console.error(`[ERROR] student/delete/:id -> ${err}`)
                res.status(400).json({
                    code: 400,
                    message:'Bad request'
                })
            }
        })
    }

    /**
     * Run
     */
    run () {
        this.middleware()
    }
}

module.exports = Delete;
