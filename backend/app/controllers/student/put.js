const studentModel = require('../../models/student.js');

class Put {
    /**
     * @constructor
     * @param {Object} app
     * @param {Object} config
     */
    constructor (app, connect, config) {
        this.app = app;
        this.studentModel = connect.model('Student', studentModel);
        this.run()
    }

    /**
     * Middleware
     */
    middleware() {
        this.app.put('/student/put/:id', (req, res) => {
            try {
                this.studentModel.updateOne({_if: req.params.id}, {...req.body, _id: req.params.id}).then((student) => {
                    res.status(200).json({student})
                }).catch(() => {
                    res.status(200).json({})
                });
            } catch(err) {
                console.error(`[ERROR] student/put/:id -> ${err}`)
                res.status(400).json({
                    code: 400,
                    message:'Bad request'
                })
            }
        })
    }

    /**
     * Run
     */
    run () {
        this.middleware()
    }
}

module.exports = Put;
