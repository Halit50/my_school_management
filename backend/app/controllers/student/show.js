const studentModel = require('../../models/student');

class Show {
    
    /**
     * @constructor
     * @param {Object} app
     * @param {Object} config
     */
    constructor (app, connect, config) {
        this.app = app;
        this.studentModel = connect.model('Student', studentModel);
        this.run()
    }

    /**
     * Middleware
     */
    middleware() {
        this.app.get('/student/show/:id', (req, res) => {
            try {
                this.studentModel.findById(req.params.id).then((student) => {
                    res.status(200).json({student} || {})
                }).catch(() => {
                    res.status(200).json({})
                });

            } catch(err) {
                console.error(`[ERROR] user/show/:id -> ${err}`)
                res.status(400).json({
                    code: 400,
                    message:'Bad request'
                })
            }
        })

        this.app.get('/students', (req, res) => {
            try {
                this.studentModel.find().then((student) => {
                    res.status(200).json({student} || {})
                }).catch(() => {
                    res.status(200).json({})
                });

            } catch(err) {
                console.error(`[ERROR] students -> ${err}`)
                res.status(400).json({
                    code: 400,
                    message:'Bad request'
                })
            }
        })

    }

    /**
     * Run
     */
    run () {
        this.middleware()
    }
}

module.exports = Show;