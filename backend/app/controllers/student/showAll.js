const studentModel = require('../../models/student');

class ShowAll {
    
    /**
     * @constructor
     * @param {Object} app
     * @param {Object} config
     */
    constructor (app, connect, config) {
        this.app = app;
        this.studentModel = connect.model('Student', studentModel);
        this.run()
    }

    /**
     * Middleware
     */
    middleware() {

        this.app.get('/students', (req, res) => {
            try {
                this.studentModel.find().then((students) => {
                    res.status(200).json({students} || {})
                }).catch(() => {
                    res.status(200).json({})
                });

            } catch(err) {
                console.error(`[ERROR] students -> ${err}`)
                res.status(400).json({
                    code: 400,
                    message:'Bad request'
                })
            }
        })

    }

    /**
     * Run
     */
    run () {
        this.middleware()
    }
}

module.exports = ShowAll;