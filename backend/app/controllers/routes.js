const Show = require('./student/show.js');
const Post = require('./student/post.js');
const Put = require('./student/put.js');
const Delete = require('./student/delete.js');
const ShowAll = require('./student/showAll');

module.exports = {
    student: {
        Show,
        Post,
        Put,
        Delete
    },
    students: {
        ShowAll
    }
}
